require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const dbConfig = require("./config/databaseConnection");
const errorMiddleware = require("./middleware/errorMiddleware");
const cors = require("cors");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const cookieParser = require("cookie-parser");
const patientModel = require("./models/Patient");
const CryptoJS = require("crypto-js");

const MONGO_URL = process.env.MONGO_URL;
const JWT_SECRET = process.env.JWT_SECRET;
const FRONTEND = process.env.FRONTEND;
const PORT = process.env.PORT || 3000;

var corsOptions = {
  origin: FRONTEND, //multiple access ['http://example.com', 'www.facebook.com']
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

const app = express();
app.use(cors(corsOptions));
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.post("/api/register", async (req, res) => {
  try {
    const password = req.body.password;
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    req.body.password = hashedPassword;
    const newuser = new studentModel(req.body);
    const student = await newuser.save();
    res.status(200).json(student);
  } catch (error) {
    res.status(500);
    // throw new Error(error.message);
  }
});

app.post("/api/login", async (req, res) => {
  const { email, password } = req.body;

  const user = await studentModel.findOne({ email });
  if (!user) {
    return res.json({ error: "User Not found" });
  }
  if (await bcrypt.compare(password, user.password)) {
    const token = jwt.sign({ email: user.email }, JWT_SECRET, {
      expiresIn: "1d",
      // expiresIn: "1hr",
      //expiresIn: "15m",
    });

    if (res.status(201)) {
      return res.json({ status: "ok", data: token });
    } else {
      return res.json({ status: "notlogin" });
    }
  }
  res.json({ status: "error", error: "Invalid Password" });
});

app.post("/api/add-patient", async (req, res) => {
  const {
    Lname,
    Fname,
    age,
    gender,
    condition,
    address,
    contactNumber,
    email,
    dateOfBirth,
    medicalHistory,
    height,
    weight,
    bloodPressure,
    heartRate,
    respiratoryRate,
    temperature,
   
    emergencyContactLName,
    emergencyContactFName,
    emergencyContactPhone,
    emergencyContactRelation,
    emergencyContactAddress,
    occupation,
    maritalStatus,
    children,
    smokingStatus,
    alcoholConsumption,
    exerciseFrequency,
  } = req.body;

  // Encrypt Lname and Fname using AES algorithm
  const encryptedLname = CryptoJS.AES.encrypt(Lname, JWT_SECRET).toString();
  const encryptedFname = CryptoJS.AES.encrypt(Fname, JWT_SECRET).toString();
    const encryptedAge = CryptoJS.AES.encrypt(
      age.toString(),
      JWT_SECRET
    ).toString();
    const encryptedGender = CryptoJS.AES.encrypt(gender, JWT_SECRET).toString();
    const encryptedCondition = CryptoJS.AES.encrypt(
      condition,
      JWT_SECRET
    ).toString();
    const encryptedAddress = CryptoJS.AES.encrypt(
      address,
      JWT_SECRET
    ).toString();
     const encryptedContactNumber = CryptoJS.AES.encrypt(
       contactNumber,
       JWT_SECRET
     ).toString();
     const encryptedEmail = CryptoJS.AES.encrypt(email, JWT_SECRET).toString();
     const encryptedDateOfBirth = CryptoJS.AES.encrypt(
       dateOfBirth,
       JWT_SECRET
     ).toString();
     const encryptedMedicalHistory = CryptoJS.AES.encrypt(
       medicalHistory,
       JWT_SECRET
     ).toString();
       const encryptedHeight = CryptoJS.AES.encrypt(
         height.toString(),
         JWT_SECRET
       ).toString();
       const encryptedWeight = CryptoJS.AES.encrypt(
         weight.toString(),
         JWT_SECRET
       ).toString();
       const encryptedBloodPressure = CryptoJS.AES.encrypt(
         bloodPressure,
         JWT_SECRET
       ).toString();
       const encryptedHeartRate = CryptoJS.AES.encrypt(
         heartRate.toString(),
         JWT_SECRET
       ).toString();
       const encryptedRespiratoryRate = CryptoJS.AES.encrypt(
         respiratoryRate.toString(),
         JWT_SECRET
       ).toString();
       const encryptedTemperature = CryptoJS.AES.encrypt(
         temperature.toString(),
         JWT_SECRET
       ).toString();
       const encryptedEmergencyContactLName = CryptoJS.AES.encrypt(
         emergencyContactLName,
         JWT_SECRET
       ).toString();
       const encryptedEmergencyContactFName = CryptoJS.AES.encrypt(
         emergencyContactFName,
         JWT_SECRET
       ).toString();
       const encryptedEmergencyContactPhone = CryptoJS.AES.encrypt(
         emergencyContactPhone,
         JWT_SECRET
       ).toString();
       const encryptedEmergencyContactRelation = CryptoJS.AES.encrypt(
         emergencyContactRelation,
         JWT_SECRET
       ).toString();
       const encryptedEmergencyContactAddress = CryptoJS.AES.encrypt(
         emergencyContactAddress,
         JWT_SECRET
       ).toString();
       const encryptedOccupation = CryptoJS.AES.encrypt(
         occupation,
         JWT_SECRET
       ).toString();
       const encryptedMaritalStatus = CryptoJS.AES.encrypt(
         maritalStatus,
         JWT_SECRET
       ).toString();
       const encryptedChildren = CryptoJS.AES.encrypt(
         children.toString(),
         JWT_SECRET
       ).toString();
       const encryptedSmokingStatus = CryptoJS.AES.encrypt(
         smokingStatus,
         JWT_SECRET
       ).toString();
       const encryptedAlcoholConsumption = CryptoJS.AES.encrypt(
         alcoholConsumption,
         JWT_SECRET
       ).toString();
       const encryptedExerciseFrequency = CryptoJS.AES.encrypt(
         exerciseFrequency,
         JWT_SECRET
       ).toString();

     
  

  try {
    await patientModel.create({
      Lname: encryptedLname,
      Fname: encryptedFname,
      age: encryptedAge,
      gender: encryptedGender,
      condition: encryptedCondition,
      address: encryptedAddress,
      contactNumber: encryptedContactNumber,
      email: encryptedEmail,
      dateOfBirth: encryptedDateOfBirth,
      medicalHistory: encryptedMedicalHistory,
      height: encryptedHeight,
      weight: encryptedWeight,
      bloodPressure: encryptedBloodPressure,
      heartRate: encryptedHeartRate,
      respiratoryRate: encryptedRespiratoryRate,
      temperature: encryptedTemperature,
      
      emergencyContactLName: encryptedEmergencyContactLName,
      emergencyContactFName: encryptedEmergencyContactFName,
      emergencyContactPhone: encryptedEmergencyContactPhone,
      emergencyContactRelation: encryptedEmergencyContactRelation,
      emergencyContactAddress: encryptedEmergencyContactAddress,
      occupation: encryptedOccupation,
      maritalStatus: encryptedMaritalStatus,
      children: encryptedChildren,
      smokingStatus: encryptedSmokingStatus,
      alcoholConsumption: encryptedAlcoholConsumption,
      exerciseFrequency: encryptedExerciseFrequency,
    });
    res.send({ status: "ok" });
  } catch (error) {
    console.error(error);
    res.status(500).send({ status: "error" });
  }
});







app.get("/api/getAllPatients", async (req, res) => {
  try {
    const allPatients = await patientModel.find({});
    res.send({ status: "ok", data: allPatients });
  } catch (error) {
    console.log(error);
  }
});




app.use(errorMiddleware);

mongoose.set("strictQuery", false);

mongoose
  .connect(MONGO_URL)
  .then(() => {
    console.log("connected to MongoDB");
    app.listen(PORT, () => {
      console.log(`Node API app is running on port ${PORT}`);
    });
  })
  .catch((error) => {
    console.log(error);
  });
