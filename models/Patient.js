const mongoose = require("mongoose");

const patientSchema = new mongoose.Schema(
  {
    Lname: {
      type: String,
    },
    Fname: {
      type: String,
    },
    age: {
      type: String,
    },
    gender: {
      type: String,
    },
    condition: {
      type: String,
    },
    address: {
      type: String,
    },
    contactNumber: {
      type: String,
    },
    email: {
      type: String,
    },
    dateOfBirth: {
      type: String,
    },
    medicalHistory: {
      type: String,
    },
    height: {
      type: String,
    },
    weight: {
      type: String,
    },
    bloodPressure: {
      type: String,
    },
    heartRate: {
      type: String,
    },
    respiratoryRate: {
      type: String,
    },
    temperature: {
      type: String,
    },
    pastMedicalConditions: {
      type: [String],
    },
    emergencyContactLName: {
      type: String,
    },
    emergencyContactFName: {
      type: String,
    },
    emergencyContactPhone: {
      type: String,
    },
    emergencyContactRelation: {
      type: String,
    },
    emergencyContactAddress: {
      type: String,
    },
    occupation: {
      type: String,
    },
    maritalStatus: {
      type: String,
    },
    children: {
      type: String,
    },
    smokingStatus: {
      type: String,
    },
    alcoholConsumption: {
      type: String,
    },
  },
  {
    collection: "tbl_patients",
    timestamps: true,
  }
);

const patientModel = mongoose.model("tbl_patients", patientSchema);
module.exports = patientModel;
