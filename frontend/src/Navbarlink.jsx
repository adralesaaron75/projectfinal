
import { Routes, Route } from 'react-router-dom'

import Notfound from './Notfound';
import PatientForm from './components/PagePatient.jsx/mainPage';
import PatientsRecords from './components/PagePatient.jsx/Records';
import PageNav from './components/PagePatient.jsx/PageNav';

function App() {
  const isUserSignedIn = !! localStorage.getItem('token');
  return (
         <Routes>
            <Route path="/" element={<PageNav />} />
           
            <Route path="/patientforms" element={<PatientForm />} />
            <Route path="/patientrecords" element={<PatientsRecords />} />
             
            <Route path="*" element={<Notfound />} />
         
         </Routes>
  )
}

export default App
