import { Link, useNavigate } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

function NavbarMenu() {
  const isUserSignedIn = !!localStorage.getItem("token");
  const navigate = useNavigate();

  const spanStyle = {
    fontSize: '14px',
    textShadow: '1px 1px 0 rgba(255, 255, 255, 0.7), -1px -1px 0 rgba(255, 255, 255, 0.7), 1px -1px 0 rgba(255, 255, 255, 0.7), -1px 1px 0 rgba(255, 255, 255, 0.7)',
  };
  
  const responsiveSpanStyle = `
    @media (max-width: 768px) {
      span {
        font-size: 10px !important;
      }
    }
  `;

  
  return (
    <>
      <style>{responsiveSpanStyle}</style>
      <Navbar expand="lg" data-bs-theme="white" style={{ background: '#75D5CA' }}>
        <Container fluid>
          <Link to="/">
            <Navbar.Brand>
              <span style={spanStyle}>
                Protecting Patient Privacy: Using AES
                Encryption in Electronic Health Records (EHR)
              </span>
            </Navbar.Brand>
          </Link>
        </Container>
      </Navbar>
    </>


  );
}

export default NavbarMenu;
