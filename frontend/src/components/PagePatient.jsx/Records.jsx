import React, { useState, useEffect } from 'react';
import axios from "axios";
import { VITE_BACKEND_URL } from "../../App";
import { toast } from "react-toastify";
import { Table, Dropdown, Card, Button, Modal, Form, OverlayTrigger, Tooltip, Container } from 'react-bootstrap';
import CryptoJS from 'crypto-js';

function PatientsRecords() {
    const [patients, setPatients] = useState([]);
    const [columns, setColumns] = useState({
        Lname: true,
        Fname: true,
        age: true,
        gender: true,
        condition: true,
        address: false,
        contactNumber: false,
        email: false,
        dateOfBirth: false,
        medicalHistory: false,
        height: false,
        weight: false,
        bloodPressure: false,
        heartRate: false,
        respiratoryRate: false,
        temperature: false,
        pastMedicalConditions: false,
        emergencyContactLName: false,
        emergencyContactFName: false,
        emergencyContactPhone: false,
        emergencyContactRelation: false,
        emergencyContactAddress: false,
        maritalStatus: false,
        smokingStatus: false,
        alcoholConsumption: false,
    });
    const [showModal, setShowModal] = useState(false);
    const [password, setPassword] = useState('');
    const [isAuthenticated, setIsAuthenticated] = useState(false);

    const correctPassword = 'hello'; // Change this to your actual password
    const JWT_SECRET = 'aaronadrales@12345!'; // Change this to your actual JWT secret

    useEffect(() => {
        getAllPatients();
    }, []);

    const getAllPatients = async () => {
        try {
            const response = await axios.get(`${VITE_BACKEND_URL}/api/getAllPatients`);
            const decryptedPatients = response.data.data.map(patient => {
                const decryptedPatient = {
                    ...patient,
                    decryptedLname: CryptoJS.AES.decrypt(patient.Lname, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedFname: CryptoJS.AES.decrypt(patient.Fname, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedAge: parseInt(CryptoJS.AES.decrypt(patient.age, JWT_SECRET).toString(CryptoJS.enc.Utf8)),
                    decryptedGender: CryptoJS.AES.decrypt(patient.gender, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedCondition: CryptoJS.AES.decrypt(patient.condition, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedAddress: CryptoJS.AES.decrypt(patient.address, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedContactNumber: CryptoJS.AES.decrypt(patient.contactNumber, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedEmail: CryptoJS.AES.decrypt(patient.email, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedDateOfBirth: CryptoJS.AES.decrypt(patient.dateOfBirth, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedMedicalHistory: CryptoJS.AES.decrypt(patient.medicalHistory, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedHeight: parseFloat(CryptoJS.AES.decrypt(patient.height, JWT_SECRET).toString(CryptoJS.enc.Utf8)),
                    decryptedWeight: parseFloat(CryptoJS.AES.decrypt(patient.weight, JWT_SECRET).toString(CryptoJS.enc.Utf8)),
                    decryptedBloodPressure: CryptoJS.AES.decrypt(patient.bloodPressure, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedHeartRate: parseInt(CryptoJS.AES.decrypt(patient.heartRate, JWT_SECRET).toString(CryptoJS.enc.Utf8)),
                    decryptedRespiratoryRate: parseInt(CryptoJS.AES.decrypt(patient.respiratoryRate, JWT_SECRET).toString(CryptoJS.enc.Utf8)),
                    decryptedTemperature: parseFloat(CryptoJS.AES.decrypt(patient.temperature, JWT_SECRET).toString(CryptoJS.enc.Utf8)),
                    decryptedPastMedicalConditions: CryptoJS.AES.decrypt(patient.pastMedicalConditions, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedEmergencyContactLName: CryptoJS.AES.decrypt(patient.emergencyContactLName, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedEmergencyContactFName: CryptoJS.AES.decrypt(patient.emergencyContactFName, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedEmergencyContactPhone: CryptoJS.AES.decrypt(patient.emergencyContactPhone, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedEmergencyContactRelation: CryptoJS.AES.decrypt(patient.emergencyContactRelation, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedEmergencyContactAddress: CryptoJS.AES.decrypt(patient.emergencyContactAddress, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedMaritalStatus: CryptoJS.AES.decrypt(patient.maritalStatus, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedSmokingStatus: CryptoJS.AES.decrypt(patient.smokingStatus, JWT_SECRET).toString(CryptoJS.enc.Utf8),
                    decryptedAlcoholConsumption: CryptoJS.AES.decrypt(patient.alcoholConsumption, JWT_SECRET).toString(CryptoJS.enc.Utf8)
                };
                return decryptedPatient;
            });
            setPatients(decryptedPatients);
        } catch (error) {
            console.error("Error fetching patients:", error);
            toast.error("Failed to fetch patients");
        }
    };

    const handleUnlock = () => {
        if (password === correctPassword) {
            setIsAuthenticated(true);
            setShowModal(false);
        } else {
            toast.error('Incorrect password');
        }
    };

    const handleLock = () => {
        setIsAuthenticated(false);
    };

    const handleColumnToggle = (column) => {
        setColumns(prevColumns => ({
            ...prevColumns,
            [column]: !prevColumns[column]
        }));
    };

    return (
        <div>
            <h2 className='mt-5' style={{marginLeft:'3%'}}>Patients Records</h2>
            <div className="d-flex justify-content-end m-3">
                <Button variant="primary" onClick={() => setShowModal(true)} style={{ marginRight: '12px' }}>Unlock Fields</Button>
                <Button variant="secondary" onClick={handleLock}>Lock Fields</Button>
            </div>
            <div>
            <Dropdown className="d-flex justify-content-end m-3">
                <Dropdown.Toggle variant="primary" id="dropdown-basic">
                    Select Columns
                </Dropdown.Toggle>

                <Dropdown.Menu>
                    {Object.keys(columns).map((column, index) => (
                        <Dropdown.Item key={index}>
                            <Form.Check
                                type="checkbox"
                                label={column}
                                checked={columns[column]}
                                onChange={() => handleColumnToggle(column)}
                            />
                        </Dropdown.Item>
                    ))}
                </Dropdown.Menu>
            </Dropdown>
            </div>


           <Container fluid style={{overflow:'scroll'}}>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        {Object.keys(columns).map((column, index) => (
                            columns[column] && <th key={index}>{column}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {patients.map((patient, index) => (
                        <tr key={index}>
                            {Object.keys(columns).map((field, idx) => (
                                columns[field] && (
                                    <td key={idx} style={{ maxWidth: '10vw', overflow: 'hidden', textOverflow: 'ellipsis' }}>
                                        <OverlayTrigger
                                            placement="top"
                                            overlay={<Tooltip id={`tooltip-${index}-${field}`}>{isAuthenticated ? patient[`decrypted${field.charAt(0).toUpperCase() + field.slice(1)}`] : patient[field]}</Tooltip>}
                                        >
                                            <span>{isAuthenticated ? patient[`decrypted${field.charAt(0).toUpperCase() + field.slice(1)}`] : patient[field]}</span>
                                        </OverlayTrigger>
                                    </td>
                                )
                            ))}
                        </tr>
                    ))}
                </tbody>
            </Table>

            </Container>

            <Modal show={showModal} onHide={() => setShowModal(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Enter Password to Unlock Fields</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => setShowModal(false)}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleUnlock}>
                        Unlock
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default PatientsRecords;
