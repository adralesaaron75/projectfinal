// src/utils/cryptoUtils.js
import CryptoJS from 'crypto-js';

const secretKey = 'lionmedics'; // Use a secure key and store it safely

export const encryptData = (data) => {
  return CryptoJS.AES.encrypt(JSON.stringify(data), secretKey).toString();
};

export const decryptData = (cipherText) => {
  const bytes = CryptoJS.AES.decrypt(cipherText, secretKey);
  return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
};
