import React, { useState } from 'react';
import { Form, Button, Container, Row, Col, Card } from 'react-bootstrap';
import axios from "axios";
import { VITE_BACKEND_URL } from "../../App";
import { toast } from "react-toastify";

const PatientForm = () => {
    const [formData, setFormData] = useState({
        Lname: '',
        Fname: '',
        age: '',
        gender: '',
        condition: '',
        address: '',
        contactNumber: '',
        email: '',
        dateOfBirth: '',
        medicalHistory: '',
        height: '',
        weight: '',
        bloodPressure: '',
        heartRate: '',
        respiratoryRate: '',
        temperature: '',
        pastMedicalConditions: [''],
        emergencyContactLName: '',
        emergencyContactFName: '',
        emergencyContactPhone: '',
        emergencyContactRelation: '',
        emergencyContactAddress: '',
        occupation: '',
        maritalStatus: '',
        children: '',
        smokingStatus: '',
        alcoholConsumption: '',
      });
      const handleChange = (e) => {
        const { name, value } = e.target;
        if (name.startsWith('pastMedicalCondition')) {
          const index = parseInt(name.split('-')[1], 10);
          const updatedConditions = [...formData.pastMedicalConditions];
          updatedConditions[index] = value;
          setFormData({
            ...formData,
            pastMedicalConditions: updatedConditions
          });
        } else {
          setFormData({
            ...formData,
            [name]: value
          });
        }
      };
      
      

  const handleAddCondition = () => {
    setFormData({
      ...formData,
      pastMedicalConditions: [...formData.pastMedicalConditions, '']
    });
  };

  const handleRemoveCondition = (index) => {
    const updatedConditions = formData.pastMedicalConditions.filter((_, i) => i !== index);
    setFormData({
      ...formData,
      pastMedicalConditions: updatedConditions
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
  
    try {
      const response = await axios.post(`${VITE_BACKEND_URL}/api/add-patient`, formData, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          "Access-Control-Allow-Origin": "*",
        },
      });
  
      if (response.data.status === "ok") {
        toast.success("Patient record added successfully");
        setTimeout(() => {
          window.location.href = "/patientrecords";
        }, 2000);
  
        // Clear the form upon successful submission
        setFormData({
          Lname: '',
          Fname: '',
          age: '',
          gender: '',
          condition: '',
          address: '',
          contactNumber: '',
          email: '',
          dateOfBirth: '',
          medicalHistory: '',
          height: '',
          weight: '',
          bloodPressure: '',
          heartRate: '',
          respiratoryRate: '',
          temperature: '',
          pastMedicalConditions: [''],
          emergencyContactLName: '',
          emergencyContactFName: '',
          emergencyContactPhone: '',
          emergencyContactRelation: '',
          emergencyContactAddress: '',
          occupation: '',
          maritalStatus: '',
          children: '',
          smokingStatus: '',
          alcoholConsumption: '',
        });
  
      } else {
        toast.error("Something went wrong");
      }
    } catch (error) {
      console.error("Error submitting patient record:", error);
      toast.error("Failed to submit patient record");
    }
  };
  
  

  return (
    <Container>
      <Row className="justify-content-center" >
        <Col md={11} className='mt-5 mb-5' >
            <Card className='p-5' style={{background:'#D2E9E9'}}>
          <h2 className="mb-4" >Patient Information</h2>
          
          <Form onSubmit={handleSubmit}>
            <Container className='p-4' style={{background:'#E3F4F4'}}>
          <div className="d-flex justify-content-between" >
            <div style={{ width: "50%", marginRight: "5px" }}>
            <Form.Group controlId="formLName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter patient's Last name" 
                name="Lname"
                value={formData.Lname}
                onChange={handleChange}
                required 
              />
            </Form.Group>
            </div>

            <div style={{ width: "50%" }}>

            <Form.Group controlId="formFName">
              <Form.Label>First Name</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter patient's First name" 
                name="Fname"
                value={formData.Fname}
                onChange={handleChange}
                required 
              />
            </Form.Group>
            </div>
            </div>


            <div className="d-flex justify-content-between mt-2">
            <div style={{ width: "85%", marginRight: "5px" }}>
            <Form.Group controlId="formAddress">
              <Form.Label>Address</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter patient's address" 
                name="address"
                value={formData.address}
                onChange={handleChange}
                required 
              />
              
            </Form.Group>
            </div>
            <div style={{ width: "15%" }}>
            <Form.Group controlId="formAge">
              <Form.Label>Age</Form.Label>
              <Form.Control 
                type="number" 
                placeholder="Age" 
                name="age"
                value={formData.age}
                onChange={handleChange}
                required 
              />
            </Form.Group>


            
            </div>
            </div>

            <div className="d-flex justify-content-between mt-2">
            <div style={{ width: "45vw", marginRight: "5px" }}>
                 <Form.Group controlId="formDateOfBirth">
              <Form.Label>Date of Birth</Form.Label>
              <Form.Control 
                type="date" 
                name="dateOfBirth"
                value={formData.dateOfBirth}
                onChange={handleChange}
                required 
              />
            </Form.Group>
                
            
            </div>




            <div style={{ width: "30vw", marginRight: "5px"}}>
                    
<Form.Group controlId="formMaritalStatus">
  <Form.Label>Marital Status</Form.Label>
  <Form.Control 
    as="select" 
    name="maritalStatus"
    value={formData.maritalStatus}
    onChange={handleChange}
    required
  >
    <option value="">Select marital status</option>
    <option value="Single">Single</option>
    <option value="Married">Married</option>
    <option value="Divorced">Divorced</option>
    <option value="Widowed">Widowed</option>
  </Form.Control>
</Form.Group>


                </div>


            <div style={{ width: "20vw"}}>
            <Form.Group controlId="formGender">
              <Form.Label>Gender</Form.Label>
              <Form.Control 
                as="select" 
                name="gender"
                value={formData.gender}
                onChange={handleChange}
                required
              >
                <option value="">Select gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Other">Other</option>
              </Form.Control>
            </Form.Group>


                </div>
                
            </div>



            <div className="d-flex justify-content-between mt-2">
            <div style={{ width: "40%", marginRight: "5px" }}>
                
            <Form.Group controlId="formContactNumber">
              <Form.Label>Contact Number</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter patient's contact number" 
                name="contactNumber"
                value={formData.contactNumber}
                onChange={handleChange}
                required 
              />
            </Form.Group>
                </div>

                <div style={{ width: "60%"}}>
                    

            <Form.Group controlId="formEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control 
                type="email" 
                placeholder="Enter patient's email" 
                name="email"
                value={formData.email}
                onChange={handleChange}
                required 
              />
            </Form.Group>
</div>

                    </div>

            </Container>

            <h2 className="mb-4 mt-4">Patient Emergency Contact</h2>
            <Container className='p-4' style={{background:'#E3F4F4'}}>

            <div className="d-flex justify-content-between">
            <div style={{ width: "50%", marginRight: "5px" }}>
            <Form.Group controlId="formEmergencyContactLName">
  <Form.Label>Emergency Contact Last Name</Form.Label>
  <Form.Control 
    type="text" 
    placeholder="Enter emergency contact's Last name" 
    name="emergencyContactLName"
    value={formData.emergencyContactLName}
    onChange={handleChange}
    required 
  />
</Form.Group>
                </div>

                <div style={{ width: "50%" }}>
            <Form.Group controlId="formEmergencyContactFName">
  <Form.Label>Emergency Contact First Name</Form.Label>
  <Form.Control 
    type="text" 
    placeholder="Enter emergency contact's First name" 
    name="emergencyContactFName"
    value={formData.emergencyContactFName}
    onChange={handleChange}
    required 
  />
</Form.Group>
                </div>

            </div>


            <div className="d-flex justify-content-between mt-2 mb-2">
            <div style={{ width: "45%", marginRight: "5px" }}>
                
<Form.Group controlId="formEmergencyContactPhone">
  <Form.Label>Emergency Contact Phone Number</Form.Label>
  <Form.Control 
    type="tel" 
    placeholder="Enter emergency contact's phone number" 
    name="emergencyContactPhone"
    value={formData.emergencyContactPhone}
    onChange={handleChange}
    required 
  />
</Form.Group>
                </div>
                <div style={{ width: "55%"}}
                
                >


                    
<Form.Group controlId="formEmergencyContactRelation">
  <Form.Label>Emergency Contact Relation</Form.Label>
  <Form.Control 
    type="text" 
    placeholder="Enter emergency contact's relation" 
    name="emergencyContactRelation"
    value={formData.emergencyContactRelation}
    onChange={handleChange}
    required 
  />
</Form.Group>
                </div>


                </div>

                
                    
<Form.Group controlId="formEmergencyContactAddress">
  <Form.Label>Emergency Contact Address</Form.Label>
  <Form.Control 
    type="text" 
    placeholder="Enter emergency contact's Address" 
    name="emergencyContactAddress"
    value={formData.emergencyContactAddress}
    onChange={handleChange}
    required 
  />
</Form.Group>
</Container>



<h2 className="mt-4 mb-4">Physical Examination</h2>
<Container className='p-4' style={{background:'#E3F4F4'}}>

<div className="d-flex justify-content-between">
    
            <div style={{ width: "50%", marginRight: "5px" }}>
            <Form.Group controlId="formHeight">
              <Form.Label>Height</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter height (e.g., 5'10'' or 178 cm)" 
                name="height"
                value={formData.height}
                onChange={handleChange}
                required 
              />
            </Form.Group>
                </div>

                <div style={{ width: "50%", marginRight: "5px" }}>
                    
            <Form.Group controlId="formWeight">
              <Form.Label>Weight</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter weight (e.g., 200 lbs or 90.7 kg)" 
                name="weight"
                value={formData.weight}
                onChange={handleChange}
                required 
              />
            </Form.Group>


                </div>
            </div>

            
<div className="d-flex justify-content-between mt-2">
            <div style={{ width: "33.3%", marginRight: "5px" }}>
                
            <Form.Group controlId="formBloodPressure">
              <Form.Label>Blood Pressure</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter blood pressure (e.g., 145/90 mmHg)" 
                name="bloodPressure"
                value={formData.bloodPressure}
                onChange={handleChange}
                required 
              />
            </Form.Group>
                </div>
                <div style={{ width: "33.3%",  marginRight: "5px" }}>
                    

            <Form.Group controlId="formHeartRate">
              <Form.Label>Heart Rate</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter heart rate (e.g., 78 bpm)" 
                name="heartRate"
                value={formData.heartRate}
                onChange={handleChange}
                required 
              />
            </Form.Group>


                </div>
                <div style={{ width: "33.3%"}}>
                    

            <Form.Group controlId="formTemperature">
              <Form.Label>Temperature</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter temperature (e.g., 98.6°F or 37°C)" 
                name="temperature"
                value={formData.temperature}
                onChange={handleChange}
                required 
              />
            </Form.Group>


                </div>

            </div>
            


        









            
            
           
           
           


          

          
          <div className="d-flex justify-content-between mt-2">
            <div style={{ width: "40%", marginRight: "5px" }}>
                

<Form.Group controlId="formSmokingStatus">
  <Form.Label>Smoking Status</Form.Label>
  <Form.Control 
    as="select" 
    name="smokingStatus"
    value={formData.smokingStatus}
    onChange={handleChange}
    required
  >
    <option value="">Select smoking status</option>
    <option value="Non-smoker">Non-smoker</option>
    <option value="Smoker">Smoker</option>
  </Form.Control>
</Form.Group>
</div>

<div style={{ width: "60%" }}>

<Form.Group controlId="formAlcoholConsumption">
  <Form.Label>Alcohol Consumption</Form.Label>
  <Form.Control 
    as="select" 
    name="alcoholConsumption"
    value={formData.alcoholConsumption}
    onChange={handleChange}
    required
  >
    <option value="">Select alcohol consumption</option>
    <option value="None">None</option>
    <option value="Occasional">Occasional</option>
    <option value="Regular">Regular</option>
  </Form.Control>
</Form.Group>
</div>
</div>
</Container>







            <h2 className="mt-4 mb-4">Medical Conditions</h2>
            
            <Container className='p-4' style={{background:'#E3F4F4'}}>
           
<Form.Group controlId="formCondition" className='mb-2'>
              <Form.Label>Medical Condition</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter patient's medical condition" 
                name="condition"
                value={formData.condition}
                onChange={handleChange}
                required 
              />
            </Form.Group>

           
            {formData.pastMedicalConditions.map((condition, index) => (
              <Form.Group key={index} controlId={`formPastMedicalCondition-${index}`}>
                <Form.Label>Condition {index + 1}</Form.Label>
                <Form.Control 
                  type="text" 
                  placeholder="Enter past medical condition (e.g., Hypertension diagnosed 2015)" 
                  name={`pastMedicalCondition-${index}`}
                  value={condition}
                  onChange={handleChange}
                  required 
                />
             

                <Button variant="danger" onClick={() => handleRemoveCondition(index)} className="mt-2 mb-4" style={{marginLeft:'5px'}}>
                  Remove Condition
                </Button>
                
              </Form.Group>
            ))}

<Row className="justify-content-end mb-2">
    <Button variant="secondary" onClick={handleAddCondition}>
      Add Condition
    </Button>
  </Row>

            

            </Container>

            <Button variant="primary" type="submit" className="mt-4" style={{float:'right'}}>
          Submit
        </Button>
          </Form>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default PatientForm;
