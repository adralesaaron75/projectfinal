import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import { FaUserPlus, FaFileAlt } from 'react-icons/fa';
import background from '../assets/images/bgroundmed.jpg';

function PageNav() {
    return (
        <div 
            className="d-flex justify-content-center align-items-center vh-100" 
            style={{
                backgroundImage: `url(${background})`, 
                backgroundSize: 'cover', 
                backgroundPosition: 'center'
            }}
        >
            <Navbar style={{ background:'#D2E9E9', padding: '5%', border:'solid 5px white'}}>
                <Nav className="flex-column flex-md-row text-center">
                    <Nav.Link as={Link} to="/patientforms" className="mb-3 mb-md-0 mx-md-3">
                        <FaUserPlus className="display-4 d-block mx-auto mb-2" />
                        <div>Add Patient Records</div>
                    </Nav.Link>
                    <Nav.Link as={Link} to="/patientrecords" className="mx-md-3">
                        <FaFileAlt className="display-4 d-block mx-auto mb-2" />
                        <div>Patient Records</div>
                    </Nav.Link>
                </Nav>
            </Navbar>
        </div>
    );
}

export default PageNav;
